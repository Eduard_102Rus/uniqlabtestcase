import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TasksListComponent } from './screens/task/tasks-list/tasks-list.component';
import { NavMenuComponent } from '../layouts/nav-menu/nav-menu.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: './screens/screens.module#ScreensModule'
  }

];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class ModulesRoutingModule { }
